/*
    ChibiOS - Copyright (C) 2006..2016 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

//#include "ch.h"
//#include "hal.h"
#include "PVS_protocol/PVS_protocol.h"
//#include "Area.h"

PVS_* g_pvs;

/*
 * Vlakno #1.
 * POSIELANIE_TUKOV na oba porty
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;

  chRegSetThreadName("send TUKs");

  uint8_t buf[20];
  uint8_t len;
  while (true) {
    palTogglePad(GPIOC, GPIOC_LED4);

    //implementova� nastavenie tuknutie na SD1;
    sdWrite(&SD1,buf,len);

    //implementova� nastavenie tuknutie na SD1;
    sdWrite(&SD2,buf,len);
    chThdSleepMilliseconds(100);
  }
}

/*
 * Vlakno #2.
 * Primanie tukov na 1. porte
 */
static THD_WORKING_AREA(waThread2, 128);
static THD_FUNCTION(Thread2, arg) {

  (void)arg;

  chRegSetThreadName("recieve 1. TUK");

  uint8_t buf[20];
  uint8_t len;
  while (true) {
    palTogglePad(GPIOC, GPIOC_LED3);
    len = sdAsynchronousRead(&SD1,buf,10);
    if (len >= 1) {
      //spracovanie spr�vy od SD1
    }
    chThdSleepMilliseconds(10);
  }
}

/*
 * Vlakno #3.
 * Primanie tukov na 2.porte
 */
static THD_WORKING_AREA(waThread3, 128);
static THD_FUNCTION(Thread3, arg) {

  (void)arg;

  chRegSetThreadName("recieve 2. TUK");


  uint8_t buf[20];
  uint8_t len;
  while (true) {
    palTogglePad(GPIOC, GPIOC_LED4);
    len = sdAsynchronousRead(&SD2,buf,10);
    if (len >= 1) {
      //spracovanie spr�vy od SD2
    }
    chThdSleepMilliseconds(10);
  }
}

/*
 * Zaciatok aplikacie
 */
int main(void) {

  // Inicializacia systemu
  // HAL inicializacia - inicializuje ovladace, dosku
  halInit();
  // Inicializacia kernelu, funkcia main() sa stane vlaknom a RTOS je ektivny
  chSysInit();




  // aktiv�cia s�riov�ch portov
  // prednastavena konfiguracia: 38400 b/s, 8, N, 1
  // pre pou�itie prednastavenej konf. pou�i� sdStart(&SDx,NULL);
  SerialConfig conf;
  conf.speed=9600;
    // Aktivacia serioveho portu 1. PA9(TX), PA10(RX)
  sdStart(&SD1, &conf);
    // Aktivacia serioveho portu 1. PA2(TX), PA3(RX)
  sdStart(&SD2, &conf);
    // Aktivacia serioveho portu 3. PB10(TX), PB11(RX)
  palSetPadMode(GPIOB,10,PAL_MODE_STM32_ALTERNATE_PUSHPULL);
  palSetPadMode(GPIOB,11,PAL_MODE_INPUT);
  sdStart(&SD3, &conf);

  // Vytvorenie vlakien
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO+1, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO+1, Thread2, NULL);
  chThdCreateStatic(waThread3, sizeof(waThread3), NORMALPRIO+1, Thread3, NULL);
  // Hlavne vlakno

  PVS_ pvs;
  g_pvs = &pvs;
  pvs_init(g_pvs, 1);


  while(true) {
    chThdSleepMilliseconds(1000);
  }

}




  /*
  uint8_t buf[20];
  uint8_t len;
  while (true) {
    len = sdAsynchronousRead(&SD3,buf,10);
    if (len >= 1) {
     *buf= *buf +1;
      sdWrite(&SD3,buf,len);
    }
    if (palReadPad(GPIOA, GPIOA_BUTTON))
    {
      sdWrite(&SD3,(uint8_t *)"Button\r\n",8);
      while(palReadPad(GPIOA, GPIOA_BUTTON))
        chThdSleepMilliseconds(10);

    }
    chThdSleepMilliseconds(10);
  }
}
  */

