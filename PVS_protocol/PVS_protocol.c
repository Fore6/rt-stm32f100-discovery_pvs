/*
 * PVS_protocol.c
 *
 *  Created on: 30.11.2018
 *      Author: pc
 */

#include "PVS_protocol.h"

uint8 pvs_tuk_init(PVS_* pvs){
  pvs->tuk_forSend.sourceAddr = pvs->myPoint.addr;
  pvs->tuk_forSend.dataLength = 4;
  pvs->tuk_forSend.addresDataLength=0;//without source addr
  pvs->tuk_forSend.addresData[0] = TUK_start;
  pvs->tuk_forSend.addresData[1] = pvs->tuk_forSend.dataLength;
  pvs->tuk_forSend.addresData[2] =  pvs->tuk_forSend.sourceAddr;
  pvs->tuk_forSend.addresData[3] = TUK_end;
  pvs->tuk_forSend.SD = &SD3;
  return 0;
}

uint8 pvs_tuk_create(PVS_TUK* tuk, SerialDriver* SD, uint8* recieveData) {
  tuk->SD = SD;
  tuk->dataLength = recieveData[1];
  tuk->sourceAddr = recieveData[2];
  tuk->addresDataLength = 0;
  for(int i = 0; i < tuk->dataLength-4; i++) {
    tuk->addresData[i] = recieveData[i+3];
    tuk->addresDataLength++;
  }
  return 0;
}

uint8 pvs_check_recieved_tuk(PVS_TUK* tuk, PVS_* pvs) {
  pvs_check_addres_in_table(tuk->sourceAddr, &pvs->points_table);
  for(int i = 0; i < tuk->addresDataLength; i++) {
    pvs_check_addres_in_table(tuk->addresData[i], &pvs->points_table);
  }
  return 0;
}

uint8 pvs_check_addres_in_table(uint8 addres, PVS_POINTS_TABLE* table) {
  for(int i=0; i < table->length; i++) {
    if ( (addres & DISTANCE_MASK) > (table->points[i].addr & DISTANCE_MASK)) {
      /*
       * bacha na toto.. je to zle treba zabezpe�i� menenie SD
       */
    }
  }
}

uint8 pvs_point_init(SerialDriver* SD, PVS_POINT* point, uint8 distance_addr) {
  point->SD = SD;
  point->addr = distance_addr;
  point->cout_waste_tuk = 0;
  point->distance = ((distance_addr & DISTANCE_MASK) >> 4);
  return 0;
}

uint8 pvs_points_table_init(PVS_* pvs) {
  pvs->points_table.length = MAX_PVS_POINT; // prerobi� ak sa bude meni� tabulka na dinamick�
  for (int i = 0; i < pvs->points_table.length; i++) {
    pvs_point_init(&SD3, &pvs->points_table.points[i], 0xFF);
  }
  return 0;
}

/*
 * init PVS_protocol
 */
uint8 pvs_init(PVS_* pvs, uint8 my_addr) {
  pvs_point_init(&SD3, &pvs->myPoint, my_addr);
  pvs_points_table_init(pvs);
  pvs_tuk_init(pvs);
  return 0;
}
