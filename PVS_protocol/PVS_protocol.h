/*
 * PVS_protocol.h
 *  ADDRES=        |  Reserve ,  D2, D1, D0, A3, A2, A1, A0 |
 *
 *   TUK_PAKET=    |  TUK_start  |  LENGTH  |  SOURCE_ADDR  |  ADDR_1  |  ADDR_2  |  ADDR_3  | ... |  ADDR_n  |  TUK_end  |
 *       B=              0             1             2           3          4               n+1         n+2
 *
 *  COMAND_PAKET=  |  COM_start  |  SOURCE_ADDR  |  DESTINATION_ADDR  |  COMAND  |  ..( |  DATA_LENGTH  |  DATA_1  |  DATA_2  | ... |  DATA_n  | )..  |  COM_end  |
 *       B=              0              1                  2               3                  (4)            (5)       (6)             (n+4)             4/(n+5)
 *
 *  Created on: 30.11.2018
 *      Author: pc
 */

#ifndef PVS_PROTOCOL_PVS_PROTOCOL_H_
#define PVS_PROTOCOL_PVS_PROTOCOL_H_

#include "ch.h"
#include "hal.h"

#define MAX_PVS_POINT 7
#define MAX_DATA_LENGTH 36
#define MAX_USEFUL_DATA_LENGTH MAX_DATA_LENGTH - 6 // 6 = riadiace bajty v comand pakete
#define DISTANCE_MASK 0x70
#define ADDR_MASK 0x0F
#define MAX_COUNT_WASTE_TUK 5

typedef enum tuk_info {
  TUK_start=1, TUK_end=0xFF, COM_start=2
}TUK_info;

typedef uint8_t uint8;

/*
 * tukanie susedov
 */
typedef struct tuk {
  uint8 sourceAddr;
  uint8 addresData[MAX_DATA_LENGTH];
  uint8 addresDataLength;
  uint8 dataLength;
  SerialDriver* SD;
}PVS_TUK;

/*
 * addr=0xFF => poin is not exsist?
 *addr=0x0F is rezerved => poin is not exsist (broadcast maybe)?
 *MSB is rezerved => poin is not exsist?
 */
typedef struct pvs_point {
  SerialDriver* SD;
  uint8 addr;
  uint8 distance;
  uint8 cout_waste_tuk;
  //statick� nie je dobr� ale pre tento pr�klad sta��
  //uint8 message[MAX_DATA_LENGTH];
  //uint8 messageLength;
}PVS_POINT;

typedef struct pvs_points_table {
  uint8 length;
  PVS_POINT points[MAX_PVS_POINT];

}PVS_POINTS_TABLE;

typedef struct PVS_protocol {
  PVS_POINT myPoint;
  PVS_POINTS_TABLE points_table;
  PVS_TUK tuk_forSend;
  //PVS_TUK tuk_forSD1;
  //PVS_TUK tuk_forSD2;
  PVS_TUK tuk_recieveTuk;
  //uint8 mutex; //(doplni� jednoduch� mutex?)
}PVS_;

/*
 * init structure tuk
 */
uint8 pvs_tuk_init(PVS_* pvs);

/*
 * create PVS_tuk from recieve data
 */
uint8 pvs_tuk_create(PVS_TUK* tuk, SerialDriver* SD, uint8* recieveData);

/*
 * return value is length of data for send
 * data is pointer where is returned data for sent
 */
uint8 pvs_tuk_getTukForSend(PVS_TUK* tuk, uint8* data);

/*
 * check point from tuk with point from PVS_table
 * aktualiyova� tabu�ku
 */
uint8 pvs_check_recieved_tuk(PVS_TUK* tuk, PVS_* pvs);

/*
 * must increment distance!!!
 */
uint8 pvs_check_addres_in_table(uint8 addres, PVS_POINTS_TABLE* table);

/*
 * init PVS_point SD -> port kde je dan� PVS_point najbli��ie,
 *                point -> smern�k na point v arraz
 *                distance_addr -> (0-3)addr (4-6)distance (7)reserved
 * prida� info do tuku;
 */
uint8 pvs_point_init(SerialDriver* SD, PVS_POINT* point, uint8 distance_addr);

/*
 * nutn� odstr�ni� info z posielaneho tuku
 */
uint8 pvs_point_deinit(PVS_POINT* point);

/*
 * init PVS_table
 */
uint8 pvs_points_table_init(PVS_* pvs);

/*
 * pvs_init use other inits function
 * tukInit()
 * arrayInit()
 * defoult addr= 0x00
 * ...
 */
uint8 pvs_init(PVS_* pvs, uint8 my_addr);

uint8 pvs_read(PVS_* pvs, SerialDriver* SD);

/*
 * t�to funkcie volaj� funkcie tuk_write a comand_write
 */
uint8 pvs_write(SerialDriver* SD, uint8 buf, uint8 len);

#endif /* PVS_PROTOCOL_PVS_PROTOCOL_H_ */

